var inicio = {
  titulo: "Agravos na Saúde do Idoso",
  local: "<br> Unidade de Saúde <br> ",
  imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_inicio.svg' class='img-case'>",
  texto: "<p class='text-center'>Olá, seja bem-vindo(a)!</p><p class='game-text-justify'>Esse jogo é composto pelos seguintes casos clínicos:<br><br>- Incontinência<br>- Doença de Parkinson e tremor<br>- Osteoporose<br>- Avaliação e Manejo Cognitivo I<br>- Avaliação e Manejo Cognitivo II<br>- Depressão<br>- Desequilíbrio e Vertigem</p><br><p class='text-center'>Boa sorte!</p>",
  callback: function() {preencherJogo(caso_incontinencia);} 
};

var caso_incontinencia = {
  titulo: "Incontinência Urinária",
  local: "Unidade de Saúde<br> Ceará<br> Segunda, 08:00",
  imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_01.svg' class='img-case'><br>Incontinência Urinária",
  texto: "<br><br><p class='game-text-justify'>Esse jogo é composto por casos clínicos resumidos, dicas que complementarão o caso, tais como dicas do médico, do paciente e do acompanhante, duas perguntas que contemplam os principais tópicos do material com alternativas e feedbacks por item, o que auxiliará no aprendizado do aluno.</p><p class='text-center'>Boa sorte!</p>",
  acoes: [
  // Caso
  {
    img: "<img src='views/unidade_3/atividade/imgs/caso_01_paciente.svg' class='img-case'><br>Elvira",
    texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Sra. Elvira, de 72 anos, veio para consulta de primeira vez com queixas de “dor nas juntas”. É hipertensa e diabética e está em uso de Propranolol, Hidroclorotiazida e Metformina. Nega outras patologias. Teve quatro gravidezes, todas a termo e com parto normal. Realizou histerectomia total aos 58 anos. Nega tabagismo ou etilismo. Indagada se perde urina sem querer em algum momento, refere que sim e que usa um pano como absorvente, mas que não falou nada por achar normal uma mulher na sua idade e com tantos filhos perder um pouco de urina. Sua pressão arterial está controlada, assim como o nível glicemico. A paciente apresenta comprometimento articular mais acentuado em mãos, joelhos e quadril, sugestivos de Osteoartrite, com sinais inflamatórios no joelho esquerdo.</p>"
  },
  // Informações geográficas
  {
    img: "<img src='views/unidade_3/atividade/imgs/caso_nordeste_local.svg' class='img-case'><br>Ceará",
    texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Nordeste</p><p class='game-text-justify'>O caso de Dona Elvira se passa em uma região onde o seu bioma é a caatinga localizado no Nordeste do Brasil. Esta vegetação é exclusiva, ou seja, não pode ser encontrado em lugar algum do planeta. A fauna da Caatinga é composta por alguns animais dentre eles o tatu-peba.</p>"
  }, 
  // Dicas
  {
    img: "<img src='views/unidade_3/atividade/imgs/caso_01.svg' class='img-case'><br>Incontinência Urinária",
    dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> A perda de urina era mais rara, mas se acentuou nos últimos três meses, e está atrapalhando a sua vida, pois está com medo de sair de casa. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> A perda de urina ocorre quando tosse ou espirra, e também às vezes sente vontade, mas não dá tempo de chegar ao banheiro. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> Paciente refere ardência miccional. </div></div></div></div></div></div>',
    menu: [
      {
        item: "Família",
        texto: "A perda de urina  era mais rara, mas se acentuou nos últimos três meses, e está atrapalhando a sua vida, pois está com medo de sair de casa."
      },
      {
        item: "Paciente",
        texto: "A perda de urina  ocorre quando tosse ou espirra, e também às vezes sente vontade, mas não dá tempo de chegar ao banheiro."
      },
      {
        item: "Médico",
        texto: "Paciente refere ardência miccional."
      }
    ]
  },
  // Atividade
  {
    texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
    callback: function() {createQuestion(1, "contentQuestion");}
  },
  // Tutorial
  {
    img: "<img src='views/unidade_3/atividade/imgs/caso_01.svg' class='img-case'><br>Incontinência Urinária",
    texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
  }
  ]
};

    // ## CASO 2 ##

    var caso_parkinson = {
      titulo: "Doença de Parkinson e Tremor",
      local: "Unidade de Saúde<br> Brasília<br> Quinta, 09:15",
      imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_02.svg' class='img-case'><br>Tremor",
      texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você está apto a enfrentar um novo desafio. O tema do caso cliníco agora é Doença de Parkinson e Tremor.</p><p class='text-center'>Boa sorte!</p>",
      acoes: [
      // Caso
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_02_paciente.svg' class='img-case'><br>Marta",
        texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Marta, 68 anos, procura atendimento médico queixando-se de tremor nas mãos. Refere que os tremores das mãos iniciaram há, aproximadamente, 3 anos e que tem se tornado cada vez mais intensos. Relata que acomete suas mãos e cabeça. Refere estar bastante preocupada, pois uma conhecida lhe falou eu poderia ser doença de Parkinson. Nega demais alterações. Desconhece comorbidades e nega uso regular de fármacos.</p>"
      },
      // Informações geográficas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_centrooeste_local.svg' class='img-case'><br>Brasília",
        texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Centro-Oeste</p><p class='game-text-justify'>O caso de Marta se passa na região Centro-Oeste. A região é a segunda maior do Brasil e faz fronteira com todas as demais regiões. Abriga a capital do país, Brasília, centro de decisões politicas.</p>"
      },
      // Dicas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_02.svg' class='img-case'><br>Tremor",
        dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> O tremor piora com a ansiedade e melhora quando ingere bebida alcoólica. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> O tremor ocorre quando estou desempenhando alguma ação. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> História familiar positiva (pai apresentava quadro semelhante). </div></div></div></div></div></div>',
        menu: [
          {
            item: "Família",
            texto: "O tremor piora com a ansiedade e melhora quando ingere bebida alcoólica."
          },
          {
            item: "Paciente",
            texto: "O tremor ocorre quando estou desempenhando alguma ação."
          },
          {
            item: "Médico",
            texto: "História familiar positiva (pai apresentava quadro semelhante)."
          }
        ]
      },
      // Atividade
      {
        texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
        callback: function() {createQuestion(3, "contentQuestion");}
      },
      // Tutorial
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_02.svg' class='img-case'><br>Tremor",
        texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
      }
      ]
    };

    // ## CASO 3 ##

    var caso_osteoporose = {
      titulo: "Osteoporose",
      local: "Unidade de Saúde<br> Rio Grande do Sul<br> Domingo, 08:43",
      imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_03.svg' class='img-case'><br>Osteoporose",
      texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você está apto a enfrentar um novo desafio. O tema do caso cliníco agora é Osteoporose.</p><p class='text-center'>Boa sorte!</p>",
      acoes: [
      // Caso
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_03_paciente.svg' class='img-case'><br>Margarida",
        texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Dona Margarida. 68 anos, hipertensa e diabética, retorna ao PSF para mostrar densitometria óssea solicitada na última consulta... Dona Margarida  relata estar ansiosa com relação ao diagnóstico de osteoporose, pois sua mãe, já falecida, era acompanhada pelo mesmo quadro e sofreu fratura de fêmur após queda da própria altura, apresentado recuperação difícil. Quando interrogada sobre medicamentos, relatar uso de prednisona, por conta própria, devido a renite alérgica, além de analgésicos esporádicos por cefaleia. </p>"
      },
      // Informações geográficas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_sul_local.svg' class='img-case'><br>Rio Grande do Sul",
        texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Sul</p><p class='game-text-justify'>O caso de Margarida se passa na região Sul. Nesta região encontra-se o maior número de migrantes Europeus principalmente de origem Alemã e Italiana. É a região com maior Índice de Desenvolvimento Humano (IDH), com as menores taxas de analfabetismo e pobreza do Brasil.</p>"
      },
      // Dicas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_03.svg' class='img-case'><br>Osteoporose",
        dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> Paciente não se alimenta bem, recusando algumas refeições. IMC <20 Kg/m2 </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> Paciente refere tabagismo pesado (50 maços/ano) e ser sedentária. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> Densitometria óssea da paciente que evidencia T- Score < - 2,5 DP. </div></div></div></div></div></div>',
        menu: [
          {
            item: "Família",
            texto: "Paciente não se alimenta bem, recusando algumas refeições. IMC <20 Kg/m2"
          },
          {
            item: "Paciente",
            texto: "Paciente refere tabagismo pesado (50 maços/ano) e ser sedentária."
          },
          {
            item: "Médico",
            texto: "Densitometria óssea da paciente que evidencia T- Score < - 2,5 DP."
          }
        ]
      },
      // Atividade
      {
        texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
        callback: function() { createQuestion(5, "contentQuestion"); }
      },
      // Tutorial
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_03.svg' class='img-case'><br>Osteoporose",
        texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
      }
      ]
    };
    
    // ## CASO 4 ##

    var caso_declinio_1 = {
	  titulo: "Avaliação e Manejo do Declínio Cognitivo I",
      local: "Unidade de Saúde<br> Acre<br> Segunda, 11:00",
      imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_declinio.svg' width='112' height='189' class='img-case'><br>Avaliação e Manejo do Declínio Cognitivo I",
      texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você está apto a enfrentar um novo desafio. O tema do caso cliníco agora é Avaliação e Manejo do Declínio Cognitivo I.</p><p class='text-center'>Boa sorte!</p>",
      acoes: [
      // Caso
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_declinio_paciente.svg' class='img-case'><br>Paciente",
        texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Paciente feminina, 72 anos, é levada a atendimento médico ambulatorial por seus filhos os quais referem esquecimento. Inicialmente esquecia onde guardava objetos, trocava nome de pessoas próximas, no entanto, o quadro foi se agravando, visto, atualmente, estar apresentando dificuldade em realizar adequadamente atividades  corriqueiras como cozinhar e tomar banho. Referem como comorbidade apenas HAS e fazer uso regular de Enalapril 20mg/dia, negando demais alterações relevantes.</p>"
      },
      // Informações demográficas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_acre_local.svg' class='img-case'><br>Acre",
        texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Norte</p><p class='game-text-justify'>Acre é um Estado que nem sempre fez parte do território brasileiro, ele foi da Bolívia por alguns anos. Em 1877 com o mercado da borracha o Acre recebeu em suas terras muito migrantes do Nordeste principalmente do Ceará os chamados seringueiros que trabalhavam com o extravio da borracha e que também lutaram pela ocupação da região.</p>"
      },
      // Dicas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_declinio.svg' width='112' height='189' class='img-case'><br>Avaliação e Manejo do Declínio Cognitivo I",
        dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> Informas, que devido a essas alterações, as atividades sociais e ocupacionais estão prejudicadas. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> Nega alteração na marcha durante esse período. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> A paciente apresenta quadro progressivo de alteração da memória e da função executiva há 8 meses. </div></div></div></div></div></div>',
        menu: [
          {
            item: "Família",
            texto: "Informas, que devido a essas alterações, as atividades sociais e ocupacionais estão prejudicadas."
          },
          {
            item: "Paciente",
            texto: "Nega alteração na marcha durante esse período."
          },
          {
            item: "Médico",
            texto: "A paciente apresenta quadro progressivo de alteração da memória e da função executiva há 8 meses."
          }
        ]
      },
      // Atividade
      {
        texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
        callback: function() {createQuestion(41, "contentQuestion");}
      },
      // Tutorial
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_declinio.svg' width='112' height='189' class='img-case'><br>Avaliação e Manejo do Declínio Cognitivo I",
        texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
      }
      ]
    };

    // ## CASO 5 ##

    var caso_declinio_2 = {
	  titulo: "Avaliação e Manejo do Declínio Cognitivo II",
      local: "Unidade de Saúde<br> Minas Gerais<br> Sexta, 14:00",
      imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_declinio_II.svg' class='img-case'><br>Avaliação e Manejo do Declínio Cognitivo II",
      texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você está apto a enfrentar um novo desafio. O tema do caso cliníco agora é Avaliação e Manejo do Declínio Cognitivo II.</p><p class='text-center'>Boa sorte!</p>",
      acoes: [
      // Caso
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_declinio_paciente_II.svg' class='img-case'><br>Paciente",
        texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Paciente masculino, 72 anos, advogado aposentado, procura atendimento médico ambulatorial, acompanhado por sua filha, com queixa principal: “meu pai está muito diferente”. Filha relata que nos últimos meses tem notado que seu pai está apresentando alterações da sua personalidade, pois sempre foi uma pessoa séria e reservada e agora tem feito uso de palavras de baixo calão, assim como comentários que jamais fazia no convívio familiar. Refere que também tem tido dificuldade em executar ações corriqueiras.</p>"
      },
      // Informações demográficas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_minas_local.svg' class='img-case'><br>Minas Gerais",
        texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Sudeste</p><p class='game-text-justify'>Minas Gerais tem uma história marcada pela Riqueza, escravidão e culturas. Com a descoberta do ouro nos séculos XVII e XVIII, o Estado abrigou muitos migrantes, imigrantes dentre eles milhares de escravos. O Pão de queijo é um referencial da cozinha mineira. Minas é um dos maiores produtores de queijo do Brasil.</p>"
      },
      // Dicas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_declinio_II.svg' class='img-case'><br>Avaliação e Manejo do Declínio Cognitivo II",
        dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> As alterações apareceram de forma gradativa. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> Nega quadro de alucinação, depressão ou ansiedade. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> Paciente não apresenta alteração de memória. </div></div></div></div></div></div>',
        menu: [
          {
            item: "Família",
            texto: "As alterações apareceram de forma gradativa."
          },
          {
            item: "Paciente",
            texto: "Nega quadro de alucinação, depressão ou ansiedade."
          },
          {
            item: "Médico",
            texto: "Paciente não apresenta alteração de memória."
          }
        ]
      },
      // Atividade
      {
        texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
        callback: function() {createQuestion(43, "contentQuestion");}
      },
      // Tutorial
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_declinio_II.svg' class='img-case'><br>Avaliação e Manejo do Declínio Cognitivo II",
        texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
      }
      ]
    };


    // ## CASO 6 ##

    var caso_depressao = {
      titulo: "Depressão",
      local: "Unidade de Saúde<br> São Paulo<br> Quarta, 15:00",
      imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_04.svg' class='img-case'><br>Depressão",
      texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você está apto a enfrentar um novo desafio. O tema do caso cliníco agora é depressão.</p><p class='text-center'>Boa sorte!</p>",
      acoes: [
      // Caso
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_04_paciente.svg' class='img-case'><br>Flávia",
        texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Flávia, 70 anos, é acompanhada por Doença de Parkinson e arritmia cardíaca. Relata desde a última consulta que se sente desmotivada e refere anedonia (diminuição da capacidade de sentir prazer ou alegria). Refere que tem ganhado peso, pois não consegue dormir a noite e se alimenta durante esse período para ajudar o tempo a passar, referindo cansaço durante o dia. Informa que tem três filhos, mas que todos já seguiram suas vidas e saíram de casa, atualmente a paciente mora apenas com a cuidadora. Paciente é ex-tabagista (30 maços/ano) e nega etilismo. Ao exame, a paciente apresenta movimentos mais vagarosos e tremor em membro superior esquerdo.</p>"
      },
      // Informações demográficas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_sudeste_local.svg' class='img-case'><br>São Paulo",
        texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Sudeste</p><p class='game-text-justify'>O caso de Dona Flávia se passa na região Sudeste. Este região é composta por uma das cidades mais populosas do mundo, São Paulo, e mais rica do Brasil. Demograficamente esta região foi marcada pela migração e imigração de várias etnias dentre eles Japoneses e Libaneses.</p>"
      },
      // Dicas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_04.svg' class='img-case'><br>Depressão",
        dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> O quadro relatado apareceu depois do diagnóstico da Doença de Parkinson há dois anos. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> Relata que sempre foi muito ansiosa e que sofre muito quando há alguma alteração na sua rotina. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> Paciente apresenta humor deprimido na maior parte do dia. </div></div></div></div></div></div>',
        menu: [
          {
            item: "Família",
            texto: "O quadro relatado apareceu depois do diagnóstico da Doença de Parkinson há dois anos."
          },
          {
            item: "Paciente",
            texto: "Relata que sempre foi muito ansiosa e que sofre muito quando há alguma alteração na sua rotina."
          },
          {
            item: "Médico",
            texto: "Paciente apresenta humor deprimido na maior parte do dia."
          }
        ]
      },
      // Atividade
      {
        texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
        callback: function() {createQuestion(7, "contentQuestion");}
      },
      // Tutorial
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_04.svg' class='img-case'><br>Depressão",
        texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
      }
      ]
    };

    // ## CASO 7 ##

    var caso_desequilibrio = {
      titulo: "Desequilíbrio e Vertigem",
      local: "Unidade de Saúde<br> Amazonas<br> Quarta, 08:00",
      imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_desequilibrio.svg' class='img-case' width='160'><br>Desequilíbrio e Vertigem",
      texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você está apto a enfrentar um novo desafio. O tema do caso cliníco agora é Desequilíbrio e Vertigem.</p><p class='text-center'>Boa sorte!</p>",
      acoes: [
      // Caso
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_04_paciente.svg' class='img-case'><br>Paciente",
        texto: "<p class='text-center game-action-title'>O caso</p><p class='game-text-justify'>Paciente feminina, 67 anos, procura atendimento médico queixando-se de tontura. Refere que o sintoma começou há, aproximadamente, 3 semanas. Relata que percebe o ambiente rodar, como se as paredes do quarto estivessem girando e que apresenta concomitantemente náuseas. Informa que o sintoma é desencadeado ao rolar na cama e calçar os sapatos. É de rápida duração, melhorando em torno de 1 minuto. Refere ser portadora apenas de osteoporose e fazer uso regular de Alendronato sódico 70mg/dia, Carbonato de cálcio 1,2g/dia e Vitamina D 800U/dia. O exame físico não demonstra alterações.</p>"
      },
      // Informações demográficas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_sudeste_local.svg' class='img-case'><br>Amazonas",
        texto: "<p class='text-center game-action-title'>Informações Geográficas</p><p class='bold'>Região Norte</p><p class='game-text-justify'>Amazonas, que faz parte da região Norte, é o local onde se passa o caso da paciente. O Estado é um dos mais extensos geograficamente do Brasil, possui a maior rede hidrográfica do país e também o maior peixe de água doce do mundo o “Pirarucu”, espécie típica do Amazonas capaz de atingir até 2,5 metros de comprimento.</p>"
      },
      // Dicas
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_04.svg' class='img-case'><br>Amazonas",
        dicatotal: '<div class="row"><div class="col-md-10 col-md-offset-1" style="background:#ebebe0;"><div class="panel-group" id="accordion"><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Família </a></h4></div><div id="collapseOne" class="panel-collapse collapse"><div class="panel-body"> Informa que frequentemente a tontura está associada a episódios de náuseas. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Paciente </a></h4></div><div id="collapseTwo" class="panel-collapse collapse"><div class="panel-body"> Nega queixas de zumbido associado. </div></div></div><div class="panel collapse-content"><div class="collapse-background text-center"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Médico </a></h4></div><div id="collapseThree" class="panel-collapse collapse"><div class="panel-body"> Quadro da paciente é de início agudo, com evolução de 3 semanas. </div></div></div></div></div></div>', 
        menu: [
          {
            item: "Família",
            texto: "Informa que frequentemente a tontura está associada a episódios de náuseas."
          },
          {
            item: "Paciente",
            texto: "Nega queixas de zumbido associado."
          },
          {
            item: "Médico",
            texto: "Quadro da paciente é de início agudo, com evolução de 3 semanas."
          }
        ]
      },
      // Atividade
      {
        texto: "<p class='text-center game-action-title'>Atividade</p><p class='game-text-justify'>Chegou a hora de testar o conhecimento adquirido! Resolva a questão ao lado, caso esteja em um computador, ou abaixo, caso esteja em um dispositivo móvel.</p>",
        callback: function() {createQuestion(33, "contentQuestion");}
      },
      // Tutorial
      {
        img: "<img src='views/unidade_3/atividade/imgs/caso_04.svg' class='img-case'><br>Amazonas",
        texto: "<p class='text-center game-action-title'>Tutorial</p><p class='game-text-justify'><ol><li>Clique no botão iniciar o caso</li><li>Leia o tutorial</li><li>Leia o caso</li><li>Leia as informações geográficas</li><li>Veja as dicas</li><li>Responda as questões</li><li>Veja os feedbacks após responder cada questão</li><li>Veja o material do assunto completo no ícone aprender</li><li>Leia e baixe o guia de bolso do assunto estudado</li><li>Veja os materiais complementares no ícone saiba mais</li></ol></p>"
      }
      ]
    };
	
var fim = {
  titulo: "Agravos na Saúde do Idoso",
  local: "<br> Unidade de Saúde <br> ",
  imgCapa: "<img src='views/unidade_3/atividade/imgs/caso_inicio.svg' class='img-case'>",
  texto: "<br><br><p class='text-center'>PARABÉNS!</p><p class='game-text-justify'>Você acaba de concluir todos os temas da unidade 03.</p><p class='text-center'>Confira as outras unidades e bons estudos!</p>",
  callback: function() {preencherJogo(caso_incontinencia);} 
};

    function preencherJogo(caso) {
      $("#medical-inquiry").show();
      

      // Containers
      var tituloContainer = $(".game-case-title");
      var localContainer = $(".game-case-location");
      var imgContainer = $(".game-case-image");
      var contentContainer = $(".game-case-content");
      var btContainer = $(".game-case-start");
      var menuContainer    = $(".game-menu");
      
      // Dados
      var tituloDoCaso     = caso.titulo;
      var localDoCaso      = caso.local;
      var imgDoCaso        = caso.imgCapa;
      var contentDoCaso    = caso.texto;
      var btInit = "";

      // Carregando os dados no container
      tituloContainer.html(tituloDoCaso);  
      localContainer.html(localDoCaso);  
      imgContainer.html(imgDoCaso);  
      contentContainer.html(contentDoCaso);  
      menuContainer.html("");
	  
	  if(caso == inicio) {
		btInit = "<button class='btn btn-customized btn-sm' onclick='preencherJogo(caso_incontinencia);'>Iniciar Jogo</button>";
		btContainer.html(btInit);
		return;
	  } else if (caso == fim) {
		btInit = "<button class='btn btn-customized btn-sm' onclick='preencherJogo(caso_incontinencia);'>Jogar novamente</button>";
		btContainer.html(btInit);
	  } else { 
		btInit = "<button class='btn btn-customized btn-sm start-game'>Iniciar Caso</button>";
		btContainer.html(btInit);
	  }

      var btCaso = $("<li class='game-bt-menu'><img src='views/unidade_3/atividade/imgs/00_caso_selected.svg' class='game-bt-link'></li>");
      var btMapa = $("<li class='game-bt-menu'><img src='views/unidade_3/atividade/imgs/02_map.svg' class='game-bt-link'></li>");
      var btDica = $("<li class='game-bt-menu'><img src='views/unidade_3/atividade/imgs/03_dica.svg' class='game-bt-link'></li>");
      var btAtiv = $("<li class='game-bt-menu'><img src='views/unidade_3/atividade/imgs/04_questoes.svg' class='game-bt-link'></li>");
      var btTuto = $("<li class='game-bt-menu'><img src='views/unidade_3/atividade/imgs/01_tutorial.svg' class='game-bt-link'></li>");

      


      // Iniciar jogo
      $(".start-game").click(function() {
        btContainer.html("");        
        menuContainer.append(btCaso, btMapa, btDica, btAtiv, btTuto);
        
        imgDoCaso = caso.acoes[0].img;
        imgContainer.html(imgDoCaso);

        contentDoCaso = caso.acoes[0].texto;
        contentContainer.html(contentDoCaso);
        addNextButton(contentContainer, "btMapa");

        $(".game-menu").on("normalize", function(){
          btCaso.find('img').attr("src", 'views/unidade_3/atividade/imgs/00_caso.svg');
          btMapa.find('img').attr("src", 'views/unidade_3/atividade/imgs/02_map.svg');
          btDica.find('img').attr("src", 'views/unidade_3/atividade/imgs/03_dica.svg');
          btAtiv.find('img').attr("src", 'views/unidade_3/atividade/imgs/04_questoes.svg');
          btTuto.find('img').attr("src", 'views/unidade_3/atividade/imgs/01_tutorial.svg');
        });

        btCaso.click(function(){
          $(this).trigger("normalize");
          $(this).find('img').attr("src", 'views/unidade_3/atividade/imgs/00_caso_selected.svg');
          
          imgDoCaso = caso.acoes[0].img;
          imgContainer.html(imgDoCaso);

          contentDoCaso = caso.acoes[0].texto;
          contentContainer.html(contentDoCaso);
          addNextButton(contentContainer, "btMapa");
        });

        btMapa.click(function(){
          $(this).trigger("normalize");
          $(this).find('img').attr("src", 'views/unidade_3/atividade/imgs/02_map_selected.svg');

          imgDoCaso = caso.acoes[1].img;
          imgContainer.html(imgDoCaso);

          contentDoCaso = caso.acoes[1].texto;
          contentContainer.html(contentDoCaso);
          addNextButton(contentContainer, "btDica");
        }); 

        btDica.click(function(){
          $(this).trigger("normalize");
          $(this).find('img').attr("src", 'views/unidade_3/atividade/imgs/03_dica_selected.svg');

          imgDoCaso = caso.acoes[2].img;
          imgContainer.html(imgDoCaso);
          contentDoCaso = caso.acoes[2].menu;
          contentDica = caso.acoes[2].dicatotal;

          // Imprimir dicas na tela.
          var dicaItem = "<p class='text-center game-action-title'>Dicas</p><div class='text-center dicas-margin'>" + contentDica;
          contentContainer.html(dicaItem);

          addNextButton(contentContainer, "btAtiv");
        });   

        btAtiv.click(function(){
          $(this).trigger("normalize");
          $(this).find('img').attr("src", 'views/unidade_3/atividade/imgs/04_questoes_selected.svg');

          contentDoCaso = caso.acoes[3].texto;
          contentContainer.html(contentDoCaso);
          caso.acoes[3].callback();

        }); 

        btTuto.click(function(){
          $(this).trigger("normalize");
          $(this).find('img').attr("src", 'views/unidade_3/atividade/imgs/01_tutorial_selected.svg');

          imgDoCaso = caso.acoes[4].img;
          imgContainer.html(imgDoCaso);

          contentDoCaso = caso.acoes[4].texto;
          contentContainer.html(contentDoCaso);
        });              
        
        var arrTreta = {btMapa: btMapa, btDica: btDica, btAtiv: btAtiv};
        function addNextButton(comp, nextLabel){
          var bt = $("<div class='text-center'><a class='btn btn-customized'>Próximo</a></div>");
          bt.click({nextLabel: nextLabel}, function(e){
            var label = e.data.nextLabel;
            arrTreta[label].trigger("click");
          });
          comp.append("<br>");
          comp.append(bt);
        }

      });
    }