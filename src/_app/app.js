/**
 * Definição dos módulos, controllers, rotas e diretivas da aplicação
 * @author Equipe de Desenvolvimento
 * @version 1.0
 * @copyright UFC/UNASUS (Nuteds)
 */

// Definindo namespace pcfa para uso global e adicionado os módulos que serão usados na aplicação
var saudeMental = angular.module('saudeMental', ['ngRoute', 'ui.bootstrap']);

/*
 * Controllers
 */

// Responsável por fazer o parser do JSON
saudeMental.controller('menuController', ['$rootScope', '$scope', '$http', '$route', function($rootScope, $scope, $http, $route) {
  $http.get('data/saudemental.json')
  .success(function(data) {
    $rootScope.unidades = data.unidades;
  })
  .error(function(data) {
    console.log("Erro ao capturar os dados do JSON");
  });
}]);

// Accordion
saudeMental.controller('accordionController', ['$scope', function($scope){
  $scope.oneAtATime = true;
}]);

// Responsável por injetar na view o arquivo correspondente a url requisitada.
saudeMental.controller('includeController', ['$scope', '$routeParams', function($scope, $routeParams, $rootScope) {
  $scope.unidadeID    = $routeParams.unidadeID;
  $scope.paginaID     = $routeParams.paginaID;
  $scope.opcao        = $routeParams.opcao;
  var path            = 'views/unidade_'+$scope.unidadeID+'/'+$scope.opcao+'/'+$scope.paginaID+'.html';
  $scope.getPartial   = function () {return path;}
}]);

/*
 * Routers
 */

saudeMental.config (['$routeProvider', function ($routeProvider) {
    $routeProvider
    .when ('/', {
      templateUrl: 'views/curso/sumario.html',
      controller: 'menuController'
    })
    .when ('/creditos', {
      templateUrl: 'views/curso/creditos.html'
    })
    .when ('/conclusao', {
      templateUrl: 'views/curso/conclusao.html'
    })
    .when('/unid_:unidadeID/:opcao/:paginaID', {
      templateUrl: 'views/curso/include.html',
      controller: 'includeController'
    })
    .when ('/atividade', {
      templateUrl: 'views/atividade/1.html'
    })
    .otherwise ({
      redirectTo: '/'
    });
    // Unidades (maneira feia. estudar melhoria)
    // for (var i = 0; i < 5; i++) {
    //   $routeProvider.when('/unid_'+i,{
    //     templateUrl: 'views/unidade_'+i+'/sumario.html',
    //     controller: 'menuController'
    //   });
    // }
}]);

/*
 * Directives
 */

// Responsável por injetar no DOM as informações/opções do conteúdo acessado (Ver)
saudeMental.directive('info', function() {
  return {
    templateUrl: 'partials/header.html',
    replace: true,
    restrict: 'E',
    scope: {
      unidade: "@unidade",
      tema: "@tema",
      unome: "@unome",
      anome: "@anome"
    }
  };
});

// Responsável pela "paginação" do curso
saudeMental.directive('paginas', function() {
  return {
    templateUrl: 'partials/pagination.html',
    replace: true,
    restrict: 'E',
    scope: {
      anterior: "@anterior",
      proximo: "@proximo",
      previous: "@previous",
      next: "@next",
      desativado: "@desativado",
      btdesativado: "@btdesativado",
	  pag: "@pag",
	  unid: "@total"
    }
  }
});

// Responsável pelo gerenciamento dos modais do curso
saudeMental.directive('modal', function() {
  return {
    templateUrl: 'partials/modal.html',
    replace: true,
    restrict: 'E',
    transclude: true,
    scope: {
      target: "@target",
      titulo: "@titulo",
      tamanho: "@tamanho"
    }
  }
});