/*
 * NUTEDS - Plugin clicar e mostrar
 * http://nuteds.ufc.br
 * 
 * Copyright (c) 2014
 */

$(function() {
  var conteudo = $('.texto-conteudo > div');
  conteudo.hide();
  

  $(conteudo[0]).show();
    $('.target').click(function() {
      $('.target div').removeClass('background-circulo');
      var obj = $(this);
      var data = obj.attr('data');
      var posicao = data - 1;
      conteudo.hide();
      $(conteudo[posicao]).show();
      $(obj).children().addClass('background-circulo');
    });
});