const mozjpeg = require('imagemin-mozjpeg');
const imageminSvgo = require('imagemin-svgo');
const imageminOptipng = require('imagemin-optipng');

module.exports =  function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		//sass
		sass: {
			dist: {
				option: {style: 'compressed'},
				files: {
					'src/assets/css/style.css' : 'src/_scss/style.scss'
				}
			}
		},
		//concatenar os arquivos js e scss
		concat: {
			js: {
				files: {
					'src/assets/js/scripts.js' : 'src/_type/*.js',
					'src/assets/lib/libs.js' : ['src/_lib/jquery.min.js','src/_lib/angular.min.js','src/_lib/bootstrap.min.js','src/_lib/angular.ui.min.js','src/_lib/angular-route.min.js']
				}
			},
			css: {
				src: 'src/_scss/*.scss',
				dest: 'src/_scss/style.scss'
			}
		},
		//minificar o js
		uglify: {
			target: {
				files: {
					'src/assets/js/scripts.min.js' : 'src/assets/js/scripts.js',
					'src/app/app.js' : 'src/_app/app.js'
				}
			}
		},
		// minificar o style.css
		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'src/assets/css',
		      src: ['style.css'],
		      dest: 'src/assets/css',
		      ext: '.min.css'
		    }]
		  }
		},
		//minificar imagens
		imagemin: {
			use: {
				options: {
	          optimizationLevel: 3,
	          svgoPlugins: [{removeViewBox: false}],
	          use: [mozjpeg(), imageminOptipng()] // Example plugin usage
	      }
			},
			target: {					
			   	files: [{
				     expand: true,
				     cwd: 'src/_img/',
				     src: ['**/*.{png,jpg,gif,svg}'],
				     dest: 'src/assets/images/'
				}]
			}
	      },
	      //apaga arquivos copiados
	      clean: {
	      	dist:{
	      		src: ['public/', 'src/_scss/style.scss', 'src/assets/css/style.css.map', 'src/assets/css/style.css', 'src/assets/js/**', 'src/assets/lib/**']
	      	},
	      },
	      //copia os arquivos
	     copy: {
     			files: 
     				{
		          expand: true,
		          cwd: 'src/',
		          src: ['**', '!_scss/**','!_type/**','!_index.html','!_views/**','!_img/**','!_partials/**','!_app/**','!_lib/**'],
		          dest: 'public/',
  		     }	          		
	     },
	     //minificar o html
	     htmlmin: {
	    	dist: {
		      options: {
				      removeComments: true,
				      collapseWhitespace: true
			     },
			     files: {
			     		'src/index.html' : 'src/_index.html'			     		
			    }
				},
				dev: {
					options: {
			      removeComments: true,
			      collapseWhitespace: true
		     },
		     files: [{
		          expand: true,
		          cwd: 'src/_views/',
		          src: ['**/*.html', '*.html'],
		          dest: 'src/views'
		     }]
	     },
	     partials: {
					options: {
			      removeComments: true,
			      collapseWhitespace: true
		     },
		     files: [{
		          expand: true,
		          cwd: 'src/_partials/',
		          src: ['**/*.html', '*.html'],
		          dest: 'src/partials'
		     }]
	     }
		},
		//verifica erro de sintax no js
		jshint: {
			src: ['src/_type/**/*.js']
		},
		//assiste as modificações
		watch: {
			src:{
    		files: ['src/**/*'],
    		tasks: ['clean', 'concat', 'sass', 'uglify','cssmin', 'htmlmin', 'jshint', 'copy']	      					
			}
  	},
  	//atualiza o browser
  	browserSync: {
  		public: {
  			src: ['src/**'],
  			options: {	    				
					watchTask: true,
					server: {
						baseDir: 'src'    						
					}
  			}
  		}
  	},
	});


	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-browser-sync');



	grunt.registerTask('server', ['browserSync', 'w']);
	grunt.registerTask('default', ['clean', 'concat', 'sass']); //'sass','copy'
	grunt.registerTask('prod', [ 'uglify','cssmin','imagemin', 'htmlmin', 'jshint','copy']);
	grunt.registerTask('w', ['watch']);

}